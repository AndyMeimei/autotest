package com.panda.autotest.core.ajax;

public class Result {
	public static final String SUCCESS_CODE = "1";// 成功代码
	public static final String ERROR_CODE = "0";// 报错代码
	private boolean success;
	private String errorMsg;
	private Object data;
	private String code;

	public Result() {
		this.success = true;
		this.code = SUCCESS_CODE;
	}

	public Result(String errorMsg) {
		this.success = false;
		this.errorMsg = errorMsg;
		this.code = ERROR_CODE;
	}

	public Result with(Object data) {
		this.data = data;
		return this;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
