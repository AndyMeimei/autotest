package com.panda.autotest.core.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.panda.autotest.core.AutoTestConstants;
import com.panda.autotest.core.parse.CaseConfig;
import com.panda.autotest.core.parse.CaseConfigManager;
import com.panda.autotest.dao.CaseDao;
import com.panda.autotest.dao.model.Case;

@Service
public class AutoTestEngine implements InitializingBean {
    private Logger logger = LoggerFactory.getLogger(AutoTestEngine.class);
    @Value("${driver.url}")
    private String driverUrl;
    @Value("${max.run.num}")
    private int maxRunNum;
    @Autowired
    private CaseConfigManager caseConfigManager;
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    private CaseHandler caseHandler;

    @Autowired
    private CaseDao caseDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        System.setProperty("webdriver.chrome.driver", driverUrl);
    }

    public void start() {
        List<String> keys = new ArrayList<String>(caseConfigManager.getCaseConfigMap().keySet());
        this.start(keys);
    }

    public void start(List<String> keys) {
        Map<String, List<CaseConfig>> groupMap = this.groupMap(keys);
        logger.info("分组信息," + groupMap);
        if (groupMap.size() <= 0) {
            return;
        }
        threadPoolTaskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                runCase(groupMap);
            }
        });
    }

    public void runCase(Map<String, List<CaseConfig>> groupMap) {
        List<String> lists = new ArrayList<String>(groupMap.keySet());
        int size = lists.size();
        int loop = size / maxRunNum;
        int index = 0;
        List<CaseConfig> otherCaseConfigList = groupMap.remove("OTHER");
        while (index <= loop) {
            int tempSize = (index == loop) ? size : maxRunNum * (index + 1);
            int count = (index == loop) ? size - index * maxRunNum : maxRunNum;
            CountDownLatch countDownLatch = new CountDownLatch(count);
            for (int i = index * maxRunNum; i < tempSize; i++) {
                threadPoolTaskExecutor
                        .execute(new CaseRunnable(countDownLatch, caseHandler, groupMap.get(lists.get(i))));
            }
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException("出错");
            }
            index++;
        }
        // OTHER的case最后跑
        if (otherCaseConfigList != null && otherCaseConfigList.size() > 0) {
            for (CaseConfig caseConfig : otherCaseConfigList) {
                try {
                    caseHandler.handle(caseConfig);
                } catch (Throwable e) {
                    logger.error("error", e);
                }
            }
        }
    }

    /**
     * @param keys
     * @return
     */
    public Map<String, List<CaseConfig>> groupMap(List<String> keys) {
        Map<String, CaseConfig> caseMap = caseConfigManager.getCaseConfigMap();
        List<CaseConfig> caseConfigList = new ArrayList<CaseConfig>();
        for (String key : keys) {
            caseConfigList.add(caseMap.get(key));
        }

        Map<String, List<CaseConfig>> groupMap = new HashMap<String, List<CaseConfig>>();
        for (CaseConfig caseConfig : caseConfigList) {
            String key = caseConfig.getLoginCompanyCode();
            if (caseConfig.getCountLogin() != 1) {
                key = "OTHER";
            }
            if (groupMap.get(key) == null) {
                groupMap.put(key, new ArrayList<CaseConfig>());
            }
            caseConfig.changeStatus(AutoTestConstants.WAIT_RUN, true);
            caseDao.updateByPrimaryKeySelective((Case) caseConfig);
            groupMap.get(key).add(caseConfig);
        }
        return groupMap;
    }
}
