package com.panda.autotest.core.engine;

public class BusException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public BusException() {
		super();
	}

	public BusException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BusException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusException(String message) {
		super(message);
	}

	public BusException(Throwable cause) {
		super(cause);
	}

}
