package com.panda.autotest.core.parse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.panda.autotest.core.AutoTestConstants;
import com.panda.autotest.dao.model.Case;

public class CaseConfig extends Case implements Serializable {

	private static final long serialVersionUID = 1L;
	private Map<String, String> settingMap;
	private List<Step> stepList;
	private String filePath;
	public List<Info> stepInfos;
	private int countLogin;// 登录次数
	private boolean forceStop;

	public boolean isForceStop() {
		return forceStop;
	}

	public synchronized void setForceStop(boolean forceStop) {
		this.forceStop = forceStop;
	}

	public int getCountLogin() {
		return countLogin;
	}

	public void setCountLogin(int countLogin) {
		this.countLogin = countLogin;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Map<String, String> getSettingMap() {
		if (settingMap == null) {
			settingMap = new HashMap<String, String>();
		}
		return settingMap;
	}

	public void setSettingMap(Map<String, String> settingMap) {
		this.settingMap = settingMap;
	}

	public List<Step> getStepList() {
		if (stepList == null) {
			stepList = new ArrayList<Step>();
		}
		return stepList;
	}

	public void setStepList(List<Step> stepList) {
		this.stepList = stepList;
	}

	public synchronized List<Info> getStepInfos() {
		if (stepInfos == null) {
			stepInfos = new ArrayList<Info>();
		}
		return stepInfos;
	}

	public synchronized void add(String str, String imgFileName) {
		Info info = new Info();
		info.setStr(str);
		if (imgFileName != null) {
			info.setImageFileName(imgFileName);
		}
		this.getStepInfos().add(info);
	}

	public synchronized void add(String str) {
		this.add(str, null);
	}

	public void setStepInfos(List<Info> stepInfos) {
		this.stepInfos = stepInfos;
	}

	public synchronized void changeStatus(String status, boolean checkWaitRun) {
		if (AutoTestConstants.RUN.equals(this.getStatus())) {
			throw new RuntimeException("已经在运行中");
		} else {
			if (checkWaitRun) {
				if (AutoTestConstants.WAIT_RUN.equals(this.getStatus())) {
					throw new RuntimeException("已经在等待运行");
				}
			}
			this.setStatus(status);
		}
	}

	public synchronized void changeStatus(String status) {
		this.setStatus(status);
	}

	public synchronized CaseConfig clearStepInfos() {
		stepInfos = new ArrayList<Info>();
		return this;
	}

}
