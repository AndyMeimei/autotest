package com.panda.autotest.core.parse;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Step implements Serializable {

	private static final long serialVersionUID = 1L;

	private Type type;
	private String desc;
	private String xpath;
	private String value;
	private String url;
	private String sql;
	private String condition;
	private String schema;
	private Map<String, String> paramMap = new HashMap<String, String>();
	private boolean afterWait = false;
	private boolean refresh = false;
	private boolean isLogin = false;
	private AtomicInteger retryNum = new AtomicInteger(0);
	private AtomicInteger countRunSql = new AtomicInteger(0);

	public synchronized void checkRetryNum(int maxRetryNum) {
		if (retryNum.get() == maxRetryNum) {
			retryNum.set(0);
			throw new RuntimeException("重试超过次数");
		}
		retryNum.getAndIncrement();
	}

	public AtomicInteger getRetryNum() {
		return retryNum;
	}

	public void setRetryNum(AtomicInteger retryNum) {
		this.retryNum = retryNum;
	}

	public AtomicInteger getCountRunSql() {
		return countRunSql;
	}

	public void setCountRunSql(AtomicInteger countRunSql) {
		this.countRunSql = countRunSql;
	}

	public boolean isLogin() {
		return isLogin;
	}

	public void setLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}

	public Map<String, String> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map<String, String> paramMap) {
		this.paramMap = paramMap;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public boolean isRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public boolean isAfterWait() {
		return afterWait;
	}

	public void setAfterWait(boolean afterWait) {
		this.afterWait = afterWait;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
