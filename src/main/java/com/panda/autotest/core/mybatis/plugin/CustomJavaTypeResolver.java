package com.panda.autotest.core.mybatis.plugin;

import java.sql.Types;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.internal.types.JavaTypeResolverDefaultImpl;

public class CustomJavaTypeResolver extends JavaTypeResolverDefaultImpl {

	@Override
	protected FullyQualifiedJavaType overrideDefaultType(IntrospectedColumn column, FullyQualifiedJavaType defaultType) {
		FullyQualifiedJavaType answer = defaultType;
		switch (column.getJdbcType()) {
		case Types.BIT:
			answer = calculateBitReplacement(column, defaultType);
			break;
		case Types.DECIMAL:
		case Types.NUMERIC:
			answer = calculateBigDecimalReplacement(column, defaultType);
			break;
		case Types.TINYINT:
			answer = new FullyQualifiedJavaType(Integer.class.getName());
			break;
		case Types.INTEGER:
			answer = new FullyQualifiedJavaType(Long.class.getName());
			break;
		}
		return answer;
	}

}
