package com.panda.autotest.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CaseExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private String orderBy;

    public CaseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderBy() {
        return orderBy;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andCaseOrderIsNull() {
            addCriterion("case_order is null");
            return (Criteria) this;
        }

        public Criteria andCaseOrderIsNotNull() {
            addCriterion("case_order is not null");
            return (Criteria) this;
        }

        public Criteria andCaseOrderEqualTo(Long value) {
            addCriterion("case_order =", value, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseOrderNotEqualTo(Long value) {
            addCriterion("case_order <>", value, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseOrderGreaterThan(Long value) {
            addCriterion("case_order >", value, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseOrderGreaterThanOrEqualTo(Long value) {
            addCriterion("case_order >=", value, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseOrderLessThan(Long value) {
            addCriterion("case_order <", value, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseOrderLessThanOrEqualTo(Long value) {
            addCriterion("case_order <=", value, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseOrderIn(List<Long> values) {
            addCriterion("case_order in", values, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseOrderNotIn(List<Long> values) {
            addCriterion("case_order not in", values, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseOrderBetween(Long value1, Long value2) {
            addCriterion("case_order between", value1, value2, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseOrderNotBetween(Long value1, Long value2) {
            addCriterion("case_order not between", value1, value2, "caseOrder");
            return (Criteria) this;
        }

        public Criteria andCaseNameIsNull() {
            addCriterion("case_name is null");
            return (Criteria) this;
        }

        public Criteria andCaseNameIsNotNull() {
            addCriterion("case_name is not null");
            return (Criteria) this;
        }

        public Criteria andCaseNameEqualTo(String value) {
            addCriterion("case_name =", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameNotEqualTo(String value) {
            addCriterion("case_name <>", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameGreaterThan(String value) {
            addCriterion("case_name >", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameGreaterThanOrEqualTo(String value) {
            addCriterion("case_name >=", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameLessThan(String value) {
            addCriterion("case_name <", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameLessThanOrEqualTo(String value) {
            addCriterion("case_name <=", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameLike(String value) {
            addCriterion("case_name like", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameNotLike(String value) {
            addCriterion("case_name not like", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameIn(List<String> values) {
            addCriterion("case_name in", values, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameNotIn(List<String> values) {
            addCriterion("case_name not in", values, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameBetween(String value1, String value2) {
            addCriterion("case_name between", value1, value2, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameNotBetween(String value1, String value2) {
            addCriterion("case_name not between", value1, value2, "caseName");
            return (Criteria) this;
        }

        public Criteria andFileNameIsNull() {
            addCriterion("file_name is null");
            return (Criteria) this;
        }

        public Criteria andFileNameIsNotNull() {
            addCriterion("file_name is not null");
            return (Criteria) this;
        }

        public Criteria andFileNameEqualTo(String value) {
            addCriterion("file_name =", value, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameNotEqualTo(String value) {
            addCriterion("file_name <>", value, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameGreaterThan(String value) {
            addCriterion("file_name >", value, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameGreaterThanOrEqualTo(String value) {
            addCriterion("file_name >=", value, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameLessThan(String value) {
            addCriterion("file_name <", value, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameLessThanOrEqualTo(String value) {
            addCriterion("file_name <=", value, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameLike(String value) {
            addCriterion("file_name like", value, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameNotLike(String value) {
            addCriterion("file_name not like", value, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameIn(List<String> values) {
            addCriterion("file_name in", values, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameNotIn(List<String> values) {
            addCriterion("file_name not in", values, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameBetween(String value1, String value2) {
            addCriterion("file_name between", value1, value2, "fileName");
            return (Criteria) this;
        }

        public Criteria andFileNameNotBetween(String value1, String value2) {
            addCriterion("file_name not between", value1, value2, "fileName");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeIsNull() {
            addCriterion("login_company_code is null");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeIsNotNull() {
            addCriterion("login_company_code is not null");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeEqualTo(String value) {
            addCriterion("login_company_code =", value, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeNotEqualTo(String value) {
            addCriterion("login_company_code <>", value, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeGreaterThan(String value) {
            addCriterion("login_company_code >", value, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeGreaterThanOrEqualTo(String value) {
            addCriterion("login_company_code >=", value, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeLessThan(String value) {
            addCriterion("login_company_code <", value, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeLessThanOrEqualTo(String value) {
            addCriterion("login_company_code <=", value, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeLike(String value) {
            addCriterion("login_company_code like", value, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeNotLike(String value) {
            addCriterion("login_company_code not like", value, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeIn(List<String> values) {
            addCriterion("login_company_code in", values, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeNotIn(List<String> values) {
            addCriterion("login_company_code not in", values, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeBetween(String value1, String value2) {
            addCriterion("login_company_code between", value1, value2, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeNotBetween(String value1, String value2) {
            addCriterion("login_company_code not between", value1, value2, "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andErrorMsgIsNull() {
            addCriterion("error_msg is null");
            return (Criteria) this;
        }

        public Criteria andErrorMsgIsNotNull() {
            addCriterion("error_msg is not null");
            return (Criteria) this;
        }

        public Criteria andErrorMsgEqualTo(String value) {
            addCriterion("error_msg =", value, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgNotEqualTo(String value) {
            addCriterion("error_msg <>", value, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgGreaterThan(String value) {
            addCriterion("error_msg >", value, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgGreaterThanOrEqualTo(String value) {
            addCriterion("error_msg >=", value, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgLessThan(String value) {
            addCriterion("error_msg <", value, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgLessThanOrEqualTo(String value) {
            addCriterion("error_msg <=", value, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgLike(String value) {
            addCriterion("error_msg like", value, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgNotLike(String value) {
            addCriterion("error_msg not like", value, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgIn(List<String> values) {
            addCriterion("error_msg in", values, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgNotIn(List<String> values) {
            addCriterion("error_msg not in", values, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgBetween(String value1, String value2) {
            addCriterion("error_msg between", value1, value2, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorMsgNotBetween(String value1, String value2) {
            addCriterion("error_msg not between", value1, value2, "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameIsNull() {
            addCriterion("error_img_name is null");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameIsNotNull() {
            addCriterion("error_img_name is not null");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameEqualTo(String value) {
            addCriterion("error_img_name =", value, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameNotEqualTo(String value) {
            addCriterion("error_img_name <>", value, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameGreaterThan(String value) {
            addCriterion("error_img_name >", value, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameGreaterThanOrEqualTo(String value) {
            addCriterion("error_img_name >=", value, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameLessThan(String value) {
            addCriterion("error_img_name <", value, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameLessThanOrEqualTo(String value) {
            addCriterion("error_img_name <=", value, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameLike(String value) {
            addCriterion("error_img_name like", value, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameNotLike(String value) {
            addCriterion("error_img_name not like", value, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameIn(List<String> values) {
            addCriterion("error_img_name in", values, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameNotIn(List<String> values) {
            addCriterion("error_img_name not in", values, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameBetween(String value1, String value2) {
            addCriterion("error_img_name between", value1, value2, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameNotBetween(String value1, String value2) {
            addCriterion("error_img_name not between", value1, value2, "errorImgName");
            return (Criteria) this;
        }

        public Criteria andInfosIsNull() {
            addCriterion("infos is null");
            return (Criteria) this;
        }

        public Criteria andInfosIsNotNull() {
            addCriterion("infos is not null");
            return (Criteria) this;
        }

        public Criteria andInfosEqualTo(String value) {
            addCriterion("infos =", value, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosNotEqualTo(String value) {
            addCriterion("infos <>", value, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosGreaterThan(String value) {
            addCriterion("infos >", value, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosGreaterThanOrEqualTo(String value) {
            addCriterion("infos >=", value, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosLessThan(String value) {
            addCriterion("infos <", value, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosLessThanOrEqualTo(String value) {
            addCriterion("infos <=", value, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosLike(String value) {
            addCriterion("infos like", value, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosNotLike(String value) {
            addCriterion("infos not like", value, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosIn(List<String> values) {
            addCriterion("infos in", values, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosNotIn(List<String> values) {
            addCriterion("infos not in", values, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosBetween(String value1, String value2) {
            addCriterion("infos between", value1, value2, "infos");
            return (Criteria) this;
        }

        public Criteria andInfosNotBetween(String value1, String value2) {
            addCriterion("infos not between", value1, value2, "infos");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeIsNull() {
            addCriterion("last_run_time is null");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeIsNotNull() {
            addCriterion("last_run_time is not null");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeEqualTo(Long value) {
            addCriterion("last_run_time =", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeNotEqualTo(Long value) {
            addCriterion("last_run_time <>", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeGreaterThan(Long value) {
            addCriterion("last_run_time >", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("last_run_time >=", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeLessThan(Long value) {
            addCriterion("last_run_time <", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeLessThanOrEqualTo(Long value) {
            addCriterion("last_run_time <=", value, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeIn(List<Long> values) {
            addCriterion("last_run_time in", values, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeNotIn(List<Long> values) {
            addCriterion("last_run_time not in", values, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeBetween(Long value1, Long value2) {
            addCriterion("last_run_time between", value1, value2, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andLastRunTimeNotBetween(Long value1, Long value2) {
            addCriterion("last_run_time not between", value1, value2, "lastRunTime");
            return (Criteria) this;
        }

        public Criteria andUseTimesIsNull() {
            addCriterion("use_times is null");
            return (Criteria) this;
        }

        public Criteria andUseTimesIsNotNull() {
            addCriterion("use_times is not null");
            return (Criteria) this;
        }

        public Criteria andUseTimesEqualTo(Double value) {
            addCriterion("use_times =", value, "useTimes");
            return (Criteria) this;
        }

        public Criteria andUseTimesNotEqualTo(Double value) {
            addCriterion("use_times <>", value, "useTimes");
            return (Criteria) this;
        }

        public Criteria andUseTimesGreaterThan(Double value) {
            addCriterion("use_times >", value, "useTimes");
            return (Criteria) this;
        }

        public Criteria andUseTimesGreaterThanOrEqualTo(Double value) {
            addCriterion("use_times >=", value, "useTimes");
            return (Criteria) this;
        }

        public Criteria andUseTimesLessThan(Double value) {
            addCriterion("use_times <", value, "useTimes");
            return (Criteria) this;
        }

        public Criteria andUseTimesLessThanOrEqualTo(Double value) {
            addCriterion("use_times <=", value, "useTimes");
            return (Criteria) this;
        }

        public Criteria andUseTimesIn(List<Double> values) {
            addCriterion("use_times in", values, "useTimes");
            return (Criteria) this;
        }

        public Criteria andUseTimesNotIn(List<Double> values) {
            addCriterion("use_times not in", values, "useTimes");
            return (Criteria) this;
        }

        public Criteria andUseTimesBetween(Double value1, Double value2) {
            addCriterion("use_times between", value1, value2, "useTimes");
            return (Criteria) this;
        }

        public Criteria andUseTimesNotBetween(Double value1, Double value2) {
            addCriterion("use_times not between", value1, value2, "useTimes");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUuidLikeInsensitive(String value) {
            addCriterion("upper(uuid) like", value.toUpperCase(), "uuid");
            return (Criteria) this;
        }

        public Criteria andCaseNameLikeInsensitive(String value) {
            addCriterion("upper(case_name) like", value.toUpperCase(), "caseName");
            return (Criteria) this;
        }

        public Criteria andFileNameLikeInsensitive(String value) {
            addCriterion("upper(file_name) like", value.toUpperCase(), "fileName");
            return (Criteria) this;
        }

        public Criteria andLoginCompanyCodeLikeInsensitive(String value) {
            addCriterion("upper(login_company_code) like", value.toUpperCase(), "loginCompanyCode");
            return (Criteria) this;
        }

        public Criteria andErrorMsgLikeInsensitive(String value) {
            addCriterion("upper(error_msg) like", value.toUpperCase(), "errorMsg");
            return (Criteria) this;
        }

        public Criteria andErrorImgNameLikeInsensitive(String value) {
            addCriterion("upper(error_img_name) like", value.toUpperCase(), "errorImgName");
            return (Criteria) this;
        }

        public Criteria andInfosLikeInsensitive(String value) {
            addCriterion("upper(infos) like", value.toUpperCase(), "infos");
            return (Criteria) this;
        }

        public Criteria andStatusLikeInsensitive(String value) {
            addCriterion("upper(status) like", value.toUpperCase(), "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}