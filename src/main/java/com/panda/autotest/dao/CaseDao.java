package com.panda.autotest.dao;

import com.panda.autotest.dao.model.Case;
import com.panda.autotest.dao.model.CaseExample;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseDao {
    long countByExample(CaseExample example);

    int deleteByPrimaryKey(String uuid);

    int insert(Case record);

    int insertSelective(Case record);

    List<Case> selectByExample(CaseExample example);

    Case selectByPrimaryKey(String uuid);

    int updateByPrimaryKeySelective(Case record);

    int updateByPrimaryKey(Case record);
}