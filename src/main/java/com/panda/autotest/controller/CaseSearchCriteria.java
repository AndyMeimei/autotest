package com.panda.autotest.controller;

import java.util.List;

import com.panda.autotest.core.BaseSearchCriteria;
import com.panda.autotest.dao.model.Case;

public class CaseSearchCriteria extends BaseSearchCriteria {
	private static final long serialVersionUID = 1L;
	private String name;
	private String fileName;
	private String siteCode;
	private String status;
	private List<Case> caseList;

	public List<Case> getCaseList() {
		return caseList;
	}

	public void setCaseList(List<Case> caseList) {
		this.caseList = caseList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
