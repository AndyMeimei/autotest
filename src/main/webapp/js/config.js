var app =  angular.module('app')
  .config(
    ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
    function ($controllerProvider,   $compileProvider,   $filterProvider,   $provide) {
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;
    }
  ])
  .config(['$translateProvider', function($translateProvider){
    //配置加载国际化文件
    $translateProvider.useStaticFilesLoader({
      prefix: 'js/l10n/',
      suffix: '.json'
    });
    //默认使用中文
    $translateProvider.preferredLanguage('zh_cn');
    //启动缓存处理
    $translateProvider.useLocalStorage();
  }])
  .config(["$httpProvider", function ($httpProvider) {
      $httpProvider.interceptors.push('httpInterceptor');
  }]);