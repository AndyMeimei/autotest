angular.module("app").directive('uiUpload', [function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs, controller) {
      var options = JSON.parse(attrs.uiUpload || {});
      var pargs = angular.extend({
        url: "file/upload",// 上传路径
        upBtn: "upBtn",
        onAdd: "onAdd",// 一个上传之前回调
        onBefore: "onBefore",// 一个上传之前回调
        onProgress: "onProgress",// 一个上传成功后回调
        onAfter: "onAfter",// 一个上传成功后回调,
        single: true,
        filters: "filters",
        pargs: "pargs"
      }, options);
      var up_pargs = pargs.pargs;
      if (typeof up_pargs == "object") {
        for ( var x in up_pargs) {
          if (angular.isString(up_pargs[x])) {
            try {
              up_pargs[x] = scope.$eval(up_pargs[x]);
            } catch (e) {
              console.warn(up_pargs[x] + "is a unlegel pargs");
            }
          }
        }
      }
      var uploader = new plupload.Uploader({
        runtimes: 'html5,flash,silverlight,html4',
        multi_selection: false,
        browse_button: element[0],
        url: pargs.url,
        filters: scope[pargs.filters] || {},
        init: {
          FileFiltered: function(uploader, file) {
            for ( var i in uploader.files) {
              if (i == uploader.files.length - 1) {
                continue;
              }
              uploader.removeFile((uploader.files)[i]);
            }
          },
          FilesAdded: function(uploader, files) { // 添加文件之后
            var handle = scope[pargs.onAdd];
            if (angular.isFunction(handle)) {
              pargs.single ? handle(files[0], uploader, up_pargs) : handle(files, uploader, up_pargs);
              if (!scope.$$phase) {
                scope.$apply();
              }
            }
          },
          BeforeUpload: function(uploader, file) { // 上传前回调
            var handle = scope[pargs.onBefore];
            if (angular.isFunction(handle)) {
              handle(file, uploader, up_pargs);
              if (!scope.$$phase) {
                scope.$apply();
              }
            }
          },
          UploadProgress: function(uploader, file) { // 上传中回调
            var handle = scope[pargs.onProgress];
            if (angular.isFunction(handle)) {
              handle(file, uploader, up_pargs);
              if (!scope.$$phase) {
                scope.$apply();
              }
            }
          },
          Error: function(uploader, err) { // 错误处理
            layer.msg(err);
          },
          FileUploaded: function(uploader, file, responseObject) { // 上传后回调
            var upFn = scope[pargs.onAfter];
            var data = angular.fromJson(responseObject.response) || {};
            if (angular.isFunction(upFn)) {
              upFn(file, data, uploader, up_pargs);
              if (!scope.$$phase) {
                scope.$apply();
              }
            }
          }
        }
      });

      uploader.init();
      // 上传按钮 // 给指定id的按钮添加点击上传
      $("button[name='" + pargs.upBtn + "']").bind('click', function() {
        uploader.start();
      });
      element.next().bind('click', function() {
        uploader.start();
      });
    }
  };
}]);