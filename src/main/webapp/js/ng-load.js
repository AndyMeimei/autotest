(function() {
  var cssFiles = [];
  var jsFiles = [];
  
  //css
  cssFiles.push("css/bootstrap.css");
  cssFiles.push("css/animate.css");
  cssFiles.push("css/font-awesome.min.css");
  cssFiles.push("css/simple-line-icons.css");
  cssFiles.push("css/font.css");
  cssFiles.push("css/app.css");
  cssFiles.push("js/modules/layer/theme/default/layer.css");
  //js
 //jQuery
  jsFiles.push("js/jquery/jquery.min.js");
  //Angular start
  jsFiles.push("js/angular/angular.js");
  jsFiles.push("js/angular/angular-animate.js");
  jsFiles.push("js/angular/angular-cookies.js");
  jsFiles.push("js/angular/angular-resource.js");
  jsFiles.push("js/angular/angular-sanitize.js");
  jsFiles.push("js/angular/angular-touch.js");
  //router
  jsFiles.push("js/angular/angular-ui-router.js");
  jsFiles.push("js/angular/ngStorage.js");
  //bootstrap
  jsFiles.push("js/angular/ui-bootstrap-tpls.js");
  jsFiles.push("js/angular/ocLazyLoad.js");
  //translate
  jsFiles.push("js/angular/angular-translate.js");
  jsFiles.push("js/angular/loader-static-files.js");
  jsFiles.push("js/angular/storage-cookie.js");
  jsFiles.push("js/angular/storage-local.js");
  //app
  jsFiles.push("js/app.js");
  jsFiles.push("js/config.js");
  jsFiles.push("js/config.lazyload.js");
  jsFiles.push("js/router/config.router.js");
  jsFiles.push("js/main.js");
  //service
  jsFiles.push("js/services/httpInterceptor.js");
  jsFiles.push("js/services/commonService.js");
  //filter
  jsFiles.push("js/filters/filters.js");
  //plugin
  jsFiles.push("js/modules/layer/layer.js");
  jsFiles.push("js/modules/bootstrap.js");
  jsFiles.push("js/modules/plupload/plupload.full.min.js");
  //directives
  jsFiles.push("js/directives/ui-focus.js");
  jsFiles.push("js/directives/ui-module.js");
  jsFiles.push("js/directives/ui-nav.js");
  jsFiles.push("js/directives/ui-scroll.js");
  jsFiles.push("js/directives/ui-shift.js");
  jsFiles.push("js/directives/ui-toggleclass.js");
  jsFiles.push("js/directives/ui-validate.js");
  jsFiles.push("js/directives/ui-table.js");
  jsFiles.push("js/directives/ui-upload.js");
  
  
  if (typeof (exports) != "undefined") {
    exports.jsFiles = jsFiles;
    exports.cssFiles = cssFiles;
  } else {
    for (var i = 0; i < cssFiles.length; i++) {
      loadCss(cssFiles[i]);
    }
    for (var i = 0; i < jsFiles.length; i++) {
      loadJs(jsFiles[i]);
    }
  }

  function loadJs(path) {
    var scriptTag = document.createElement("script");
    scriptTag.type = "text/javascript";
    scriptTag.src = path;
    document.write(outerHTML(scriptTag));
  }

  function outerHTML(node) {
    return node.outerHTML || (function(n) {
      var div = document.createElement('div'), h;
      div.appendChild(n);
      h = div.innerHTML;
      div = null;
      return h;
    })(node);
  }

  function loadCss(path) {
    var cssLink = document.createElement("link");
    cssLink.rel = "stylesheet";
    cssLink.type = "text/css";
    cssLink.href = path;
    document.getElementsByTagName("head")[0].appendChild(cssLink);
  }
})();